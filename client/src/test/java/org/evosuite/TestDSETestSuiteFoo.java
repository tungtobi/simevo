/*
 * Copyright (C) 2010-2018 Gordon Fraser, Andrea Arcuri and EvoSuite
 * contributors
 *
 * This file is part of EvoSuite.
 *
 * EvoSuite is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3.0 of the License, or
 * (at your option) any later version.
 *
 * EvoSuite is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with EvoSuite. If not, see <http://www.gnu.org/licenses/>.
 */
package org.evosuite;

import com.examples.with.different.packagename.symbolic.Foo;
import com.examples.with.different.packagename.symbolic.Fraction;
import org.evosuite.Properties.Criterion;
import org.evosuite.classpath.ClassPathHandler;
import org.evosuite.result.TestGenerationResult;
import org.evosuite.setup.DependencyAnalysis;
import org.evosuite.symbolic.TestCaseBuilder;
import org.evosuite.testcase.DefaultTestCase;
import org.evosuite.testcase.TestChromosome;
import org.evosuite.testcase.execution.ExecutionResult;
import org.evosuite.testcase.execution.TestCaseExecutor;
import org.evosuite.testcase.factories.RandomLengthTestFactory;
import org.evosuite.testcase.variable.VariableReference;
import org.evosuite.utils.LoggingUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TestDSETestSuiteFoo {

    /**
     * Creates the test case:
     *
     * <code>
     * int int0 = 10;
     * int int1 = 10;
     * int int2 = 10;
     * Foo.bar(int0,int1,int2);
     * </code>
     *
     * @return
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws ClassNotFoundException
     */
    private static DefaultTestCase buildTestCase0()
        throws NoSuchMethodException, SecurityException, ClassNotFoundException {
        TestCaseBuilder builder = new TestCaseBuilder();
        VariableReference int0 = builder.appendIntPrimitive(10);
        VariableReference int1 = builder.appendIntPrimitive(10);
        VariableReference int2 = builder.appendIntPrimitive(10);
        Class<?> fooClass = TestGenerationContext.getInstance().getClassLoaderForSUT()
            .loadClass(Properties.TARGET_CLASS);
        Method barMethod = fooClass.getMethod("bar", int.class, int.class, int.class);
        builder.appendMethod(null, barMethod, int0, int1, int2);
        return builder.getDefaultTestCase();
    }

    private static final long DEFAULT_LOCAL_SEARCH_BUDGET = Properties.LOCAL_SEARCH_BUDGET;
    private static final Properties.LocalSearchBudgetType DEFAULT_LOCAL_SEARCH_BUDGET_TYPE = Properties.LOCAL_SEARCH_BUDGET_TYPE;
    private static final Properties.SolverType DEFAULT_DSE_SOLVER = Properties.DSE_SOLVER;
    private static final Properties.DSEType DEFAULT_LOCAL_SEARCH_DSE = Properties.LOCAL_SEARCH_DSE;
    private static final double DEFAULT_DSE_PROBABILITY = Properties.DSE_PROBABILITY;

    @Before
    public void init() {
        ClassPathHandler.getInstance().changeTargetCPtoTheSameAsEvoSuite();
        Properties.LOCAL_SEARCH_BUDGET = Integer.MAX_VALUE;
        Properties.LOCAL_SEARCH_BUDGET_TYPE = Properties.LocalSearchBudgetType.TESTS;
        Properties.DSE_PROBABILITY = 1.0;
        Properties.LOCAL_SEARCH_DSE = Properties.DSEType.SUITE;
    }

    @After
    public void restoreProperties() {
        Properties.LOCAL_SEARCH_BUDGET = DEFAULT_LOCAL_SEARCH_BUDGET;
        Properties.LOCAL_SEARCH_BUDGET_TYPE = DEFAULT_LOCAL_SEARCH_BUDGET_TYPE;
        Properties.DSE_SOLVER = DEFAULT_DSE_SOLVER;
        Properties.DSE_PROBABILITY = DEFAULT_DSE_PROBABILITY;
        Properties.LOCAL_SEARCH_DSE = DEFAULT_LOCAL_SEARCH_DSE;
    }

    @Test
    public void testMOSA() throws ClassNotFoundException {
        Properties.DSE_SOLVER = Properties.SolverType.EVOSUITE_SOLVER;
        Properties.CRITERION = new Properties.Criterion[]{Criterion.BRANCH};
        Properties.TARGET_CLASS = Fraction.class.getName();

        TestGenerationContext.getInstance().getClassLoaderForSUT().loadClass(Properties.TARGET_CLASS);

        TestSuiteGenerator generator = new TestSuiteGenerator();
        TestGenerationResult result = generator.generateTestSuite();

        assertEquals(result.getTestGenerationStatus(), TestGenerationResult.Status.SUCCESS);

        assertEquals(result.getUncoveredBranches().size(), 0);
        assertEquals(result.getCoveredBranches().size(), 10);

        System.out.println();
    }

    @Test
    public void generateRandomTestCase() throws Throwable {
        Properties.DSE_SOLVER = Properties.SolverType.EVOSUITE_SOLVER;
        Properties.CRITERION = new Properties.Criterion[]{Criterion.BRANCH};
        Properties.TARGET_CLASS = Fraction.class.getName();

        TestGenerationContext.getInstance().getClassLoaderForSUT().loadClass(Properties.TARGET_CLASS);

        initializeTargetClass();

        RandomLengthTestFactory factory = new RandomLengthTestFactory();
        TestChromosome chromosome = factory.getChromosome();

        System.out.println();
    }

    private void initializeTargetClass() throws ClassNotFoundException {
        String cp = ClassPathHandler.getInstance().getTargetProjectClasspath();

        // Generate inheritance tree and call graph *before* loading the CUT
        // as these are required for instrumentation for context-sensitive
        // criteria (e.g. ibranch)
        DependencyAnalysis.initInheritanceTree(Arrays.asList(cp.split(File.pathSeparator)));
        DependencyAnalysis.initCallGraph(Properties.TARGET_CLASS);

        // Analysis has to happen *after* the CUT is loaded since it will cause
        // several other classes to be loaded (including the CUT), but we require
        // the CUT to be loaded first
        DependencyAnalysis.analyzeClass(Properties.TARGET_CLASS, Arrays.asList(cp.split(File.pathSeparator)));
        LoggingUtils.getEvoLogger().info("* " + ClientProcess.getPrettyPrintIdentifier() + "Finished analyzing classpath");
    }

//    @Test
//    public void testAVMSolver() throws NoSuchMethodException, SecurityException, ClassNotFoundException {
//
//        Properties.DSE_SOLVER = Properties.SolverType.EVOSUITE_SOLVER;
//        Properties.CRITERION = new Properties.Criterion[]{Criterion.BRANCH};
//        Properties.TARGET_CLASS = Foo.class.getName();
//
//        TestGenerationContext.getInstance().getClassLoaderForSUT().loadClass(Properties.TARGET_CLASS);
//
//        BranchCoverageSuiteFitness branchCoverageSuiteFitness = new BranchCoverageSuiteFitness();
//        TestSuiteChromosome suite = new TestSuiteChromosome();
//        suite.addFitness(branchCoverageSuiteFitness);
//        branchCoverageSuiteFitness.getFitness(suite);
//
//        // no goals covered yet
//        int coveredGoals0 = suite.getNumOfCoveredGoals();
//        int notCoveredGoals0 = suite.getNumOfNotCoveredGoals();
//        assertEquals(0, coveredGoals0);
//        assertNotEquals(0, notCoveredGoals0);
//
//        DefaultTestCase testCase0 = buildTestCase0();
//        TestChromosome testChromosome0 = new TestChromosome();
//        testChromosome0.setTestCase(testCase0);
//        suite.addTest(testChromosome0);
//
//        double fitnessBeforeLocalSearch = branchCoverageSuiteFitness.getFitness(suite);
//        int coveredGoalsBeforeLocalSearch = suite.getNumOfCoveredGoals();
//
//        // some goal was covered
//        assertTrue(coveredGoalsBeforeLocalSearch > 0);
//
//        LocalSearchObjective<TestSuiteChromosome> localSearchObjective = new DefaultLocalSearchObjective<>();
//        localSearchObjective.addFitnessFunction(branchCoverageSuiteFitness);
//
//        TestSuiteLocalSearch localSearch = new TestSuiteLocalSearch();
//
//        boolean improved;
//        do {
//            improved = localSearch.doSearch(suite, localSearchObjective);
//        } while (improved);
//
//        double fitnessAfterLocalSearch = branchCoverageSuiteFitness.getFitness(suite);
//        int coveredGoalsAfterLocalSearch = suite.getNumOfCoveredGoals();
//
//        assertTrue(fitnessAfterLocalSearch < fitnessBeforeLocalSearch);
//        assertTrue(coveredGoalsAfterLocalSearch > coveredGoalsBeforeLocalSearch);
//
//        int finalSuiteSize = suite.size();
//        assertTrue(coveredGoalsAfterLocalSearch >= 7);
//        assertTrue(finalSuiteSize >= 4);
//    }
//
//    @Test
//    public void testCVC4Solver() throws NoSuchMethodException, SecurityException, ClassNotFoundException {
//        String cvc4_path = System.getenv("cvc4_path");
//        if (cvc4_path != null) {
//            Properties.CVC4_PATH = cvc4_path;
//        }
//        Assume.assumeTrue(Properties.CVC4_PATH != null);
//        Properties.DSE_SOLVER = Properties.SolverType.CVC4_SOLVER;
//        Properties.CRITERION = new Properties.Criterion[]{Criterion.BRANCH};
//        Properties.TARGET_CLASS = Foo.class.getName();
//
//        TestGenerationContext.getInstance().getClassLoaderForSUT().loadClass(Properties.TARGET_CLASS);
//
//        BranchCoverageSuiteFitness branchCoverageSuiteFitness = new BranchCoverageSuiteFitness();
//        TestSuiteChromosome suite = new TestSuiteChromosome();
//        suite.addFitness(branchCoverageSuiteFitness);
//        branchCoverageSuiteFitness.getFitness(suite);
//
//        // no goals covered yet
//        int coveredGoals0 = suite.getNumOfCoveredGoals();
//        int notCoveredGoals0 = suite.getNumOfNotCoveredGoals();
//        assertEquals(0, coveredGoals0);
//        assertNotEquals(0, notCoveredGoals0);
//
//        DefaultTestCase testCase0 = buildTestCase0();
//        TestChromosome testChromosome0 = new TestChromosome();
//        testChromosome0.setTestCase(testCase0);
//        suite.addTest(testChromosome0);
//
//        double fitnessBeforeLocalSearch = branchCoverageSuiteFitness.getFitness(suite);
//        int coveredGoalsBeforeLocalSearch = suite.getNumOfCoveredGoals();
//
//        // some goal was covered
//        assertTrue(coveredGoalsBeforeLocalSearch > 0);
//
//        LocalSearchObjective<TestSuiteChromosome> localSearchObjective = new DefaultLocalSearchObjective<>();
//        localSearchObjective.addFitnessFunction(branchCoverageSuiteFitness);
//
//        TestSuiteLocalSearch localSearch = new TestSuiteLocalSearch();
//        boolean improved;
//        do {
//            improved = localSearch.doSearch(suite, localSearchObjective);
//        } while (improved);
//
//
//        double fitnessAfterLocalSearch = branchCoverageSuiteFitness.getFitness(suite);
//        int coveredGoalsAfterLocalSearch = suite.getNumOfCoveredGoals();
//
//        assertTrue(fitnessAfterLocalSearch < fitnessBeforeLocalSearch);
//        assertTrue(coveredGoalsAfterLocalSearch > coveredGoalsBeforeLocalSearch);
//
//        int finalSuiteSize = suite.size();
//        assertEquals(8, coveredGoalsAfterLocalSearch);
//        assertTrue(finalSuiteSize >= 5);
//    }

}
