/*
 * Copyright (C) 2010-2018 Gordon Fraser, Andrea Arcuri and EvoSuite
 * contributors
 *
 * This file is part of EvoSuite.
 *
 * EvoSuite is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3.0 of the License, or
 * (at your option) any later version.
 *
 * EvoSuite is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with EvoSuite. If not, see <http://www.gnu.org/licenses/>.
 */
package org.evosuite.coverage.io;

/**
 * @author Jose Miguel Rojas
 */
public interface IOCoverageConstants {
    String CHAR_ALPHA = "AlphabeticChar";
    String CHAR_DIGIT = "Digit";
    String CHAR_OTHER = "OtherChar";
    String BOOL_TRUE = "True";
    String BOOL_FALSE = "False";
    String NUM_POSITIVE = "Positive";
    String NUM_ZERO = "Zero";
    String NUM_NEGATIVE = "Negative";
    String REF_NULL = "Null";
    String REF_NONNULL = "NonNull";
    String ARRAY_EMPTY = "EmptyArray";
    String ARRAY_NONEMPTY = "NonEmptyArray";
    String STRING_EMPTY = "EmptyString";
    String STRING_NONEMPTY = "NonEmptyString";
    String LIST_EMPTY = "EmptyList";
    String LIST_NONEMPTY = "NonEmptyList";
    String SET_EMPTY = "EmptySet";
    String SET_NONEMPTY = "NonEmptySet";
    String MAP_EMPTY = "EmptyMap";
    String MAP_NONEMPTY = "NonEmptyMap";
}
