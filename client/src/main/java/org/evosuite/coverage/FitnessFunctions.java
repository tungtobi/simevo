/*
 * Copyright (C) 2010-2018 Gordon Fraser, Andrea Arcuri and EvoSuite
 * contributors
 *
 * This file is part of EvoSuite.
 *
 * EvoSuite is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3.0 of the License, or
 * (at your option) any later version.
 *
 * EvoSuite is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with EvoSuite. If not, see <http://www.gnu.org/licenses/>.
 */
package org.evosuite.coverage;

import org.evosuite.Properties;
import org.evosuite.Properties.Criterion;
import org.evosuite.coverage.branch.BranchCoverageFactory;
import org.evosuite.coverage.branch.BranchCoverageSuiteFitness;
import org.evosuite.coverage.branch.BranchCoverageTestFitness;
import org.evosuite.testcase.TestFitnessFunction;
import org.evosuite.testsuite.TestSuiteFitnessFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * factory class for fitness functions
 *
 * @author mattia
 */
public class FitnessFunctions {

    private static final Logger logger = LoggerFactory.getLogger(FitnessFunctions.class);

    /**
     * <p>
     * getFitnessFunction
     * </p>
     * TODO: rollbback
     * @param criterion a {@link Criterion} object.
     * @return a {@link TestSuiteFitnessFunction} object.
     */
    public static TestSuiteFitnessFunction getFitnessFunction(Criterion criterion) {
        switch (criterion) {
            case BRANCH:
                return new BranchCoverageSuiteFitness();
            default:
                logger.warn("No TestSuiteFitnessFunction defined for {}; using default one (BranchCoverageSuiteFitness)", Arrays.toString(Properties.CRITERION));
                return new BranchCoverageSuiteFitness();
        }
    }

    /**
     * <p>
     * getFitnessFactory
     * </p>
     *
     * @param crit a {@link Criterion} object.
     * @return a {@link TestFitnessFactory} object.
     */
    public static TestFitnessFactory<? extends TestFitnessFunction> getFitnessFactory(
            Criterion crit) {
        switch (crit) {
            case BRANCH:
                return new BranchCoverageFactory();
            default:
                logger.warn("No TestFitnessFactory defined for " + crit
                        + " using default one (BranchCoverageFactory)");
                return new BranchCoverageFactory();
        }
    }

    /**
     * Converts a {@link Criterion} object to a
     * {@link TestFitnessFunction} class.
     *
     * @param criterion a {@link Criterion} object.
     * @return a {@link Class} object.
     */
    public static Class<?> getTestFitnessFunctionClass(Criterion criterion) {
        switch (criterion) {
            case BRANCH:
                return BranchCoverageTestFitness.class;
            default:
                throw new RuntimeException("No criterion defined for " + criterion.name());
        }
    }

}
